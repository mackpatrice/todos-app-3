import React, { useState } from "react";
import { connect } from "react-redux";

import { Link, Switch, Route } from "react-router-dom";

import { addTodo, clearCompleted } from "./actions";
import TodoList from "./components/TodoList";

//pass reducer
const App = ({ todos, addTodo, clearCompleted }) => {
  const [newText, setNewText] = useState("");

  const handleAddTodo = (e) => {
    if (e.keyCode === 13) {
        addTodo(newText); 
        setNewText("");  
  }
  };


  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          name="newText"
          value={newText}
          onChange={(e) => setNewText(e.target.value)}
          onKeyDown={handleAddTodo}
          placeholder="What needs to be done?"
          autofocus
        />
      </header>
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => 
            <TodoList
            {...props} 
            todos={todos} 
            />
          }
        />
        <Route
          path="/active"
          render={(props) => (
            <TodoList
              {...props}
              todos={todos.filter((todo) => !todo.completed)}
            />
          )}
        />
        <Route
          path="/completed"
          render={(props) => (
            <TodoList
              {...props}
              todos={todos.filter((todo) => !todo.completed)}
            />
          )}
        />
      </Switch>
      <footer className="footer">
        <span className="todo-count">
          <strong>{todos.filter((todo) => !todo.completed).length}</strong>{""} 
          item(s) left
        </span>
        <ul className="filters">
          <li>
            <Link to="/">All</Link>
          </li>
          <li>
            <Link to="/active">Active</Link>
          </li>
          <li>
            <Link to="/completed">Completed</Link>
          </li>
        </ul>
        <button className="clear-completed" onClick={clearCompleted}>
          Clear completed
        </button>
      </footer>
    </section>
  );
};

const mapStateToProps = (state) => ({
  todos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (newText) => dispatch(addTodo(newText)),
  clearCompleted: () => dispatch(clearCompleted()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
